# Login Module for Pseudonym Ecosystem
The Login Module for the MTLG provides an easy to use interface for players to login or create accounts. The corresponding backend is handled by the Identity-Server and Pseudonym Provider. After logging in, the game developer can access the players nickname for customizing the game and the pseudonym to provide personalized, yet anonymous learning analytics data to the LRS.

## Usage

#### Called by Game
* `showLogin()` Displays all Login Windows (if no custom windows are defined, it will display 4 default windows)
* `hideLogin()` Hides all Login Windows
* `showLoginWindow(index)` Displays the specified Login Window
* `hideLoginWindow(index)` Hides the specified Login Window
* `createLoginWindow(index, area, position_identifier)` Creates a new Login Window, the area parameter may look like this:
    ```
    area: {
        middleX: MTLG.getOptions().width*3/4,
        middleY: MTLG.getOptions().height / 4,
        sizeX: MTLG.getOptions().width/3,
        sizeY: MTLG.getOptions().height/3,
        rotation: 180
    }
    ```

* `deleteLoginWindow(index)` Deletes a Login Window



#### Access Data from logged-in Users
* `MTLG.getPlayers()` Returns Array containing all Playerobjects
* `MTLG.getPlayer(index)` Returns Playerobject with
    * `.pseudonym` Unique pseudonym of this user
    * `.nickname` Choosen Nickname of this user
    * `.position` Identifier to match players to areas of the game where the Login Window was displayed

#### Called from within MTLG (not relevant for game devs)
* `init()` Tries to load configuration from Identity-Server for specific game
* `info()` Displays information about module


## Settings
Hierachically Appraoch
1. Settings from Identitiy-Server
2. Settings from Manifest/game.config
3. Fallback Settings of Module

##### Important Module Settings
* `host:` URL of Identitiy-Server (without http://, e.g. 'mtlg.elearn.rwth-aachen.de/ident')
* `name:` Display Name of Server
* `game_id:` Unique UUID identifiying the game (e.g. "f13d426b-cf51-4db8-b6c2-a1a18bb11ec1")

##### Game Specific Module Settings
- `input_method` Hardware, can be 'qr' or 'virtual' or 'hardware' (look into Utils Module for further information)
- `windowcolor` Window background color , e.g. "white"
- `buttoncolor1` Buttoncolor scheme
- `buttoncolor2` Buttoncolor scheme
- `textcolor` Textcolor, e.g. "black"

###### Additional Settings
Overwritten by Identserver settings as a security measurement so you can´t access names or login without pw if it´s not wanted by the admin
* `listable_names` If true players are displayed a list of all available user accounts (instead of manually inserting nickname)
* `login_withoutpw` If true players can login without password (meaning it`s only based on the nickname)
* `login_required` If true players can not close the login window prematurely
* `anonymously` If true players can chose to stay anonymous (which means they get a pseudonym without account creation)
* `login_methods` List of available login methods for the user, custom login methods can be created on the login server

###### Login Method Specific Settings
You can set custom symbols and questions fitting your game. The symbols must be unicode emojies (which you can find here: https://unicode.org/emoji/charts/full-emoji-list.html). You can add arbitary number of symbols as a response for each question. Keep in mind that this will cause problems if you want to use the same login method for the same account between different games with different symbols. To do this you´ll have to create a new login method on the Identy-Server
* symbols: [['🥪','🍣','🍰','🍝','🍕','🍔'],['🐶','🐷','🐻','🐸'],[['🤟'],['🤙'],['🤏'],['🖖'],['🤲'],['👍'],['👎'],['🖐']]],
* symbols_categories: ["What's your favorite food?","Which animal do you like?","What's your secret hand sign?"],


Example Usage can be found in the corresponding demo.
