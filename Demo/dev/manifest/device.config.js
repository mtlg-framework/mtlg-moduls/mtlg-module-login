MTLG.loadOptions({
  "zoll":27, //screen size in inches
  "mockLogin":false, //set to true to skip login procedure
  "mockNumber":2, //number of generated fake logins if mockLogin is true
  "language":"en", //Highest priority language setting, device dependant
});
