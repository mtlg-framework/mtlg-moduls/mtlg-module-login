
function showcaseLogin(){


    //Show default player1 (at index 0)
    MTLG.login.showLoginPlayer(0)

    //Creates new login window location for player2 (at index 1)
    MTLG.login.createLoginPlayer(1,{
        middleX: MTLG.getOptions().width*3/4,
        middleY: MTLG.getOptions().height * 3/4,
        sizeX: MTLG.getOptions().width/3,
        sizeY: MTLG.getOptions().height/3,
        rotation: 0}, 2)

    //Shows all existint login windows (there are 4 default players)
    //MTLG.login.showLogin()


}
