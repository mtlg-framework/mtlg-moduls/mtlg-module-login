const axios = require('axios');
const sha256 = require('tiny-hashes/sha256');

//Inits Module
var init = function(options) {

  //get Config from manifest/game.config  and overwrite default values
  Object.assign(login_config, options.login)

  //get Config from Identityserver
  function setConfig(reply){
    Object.assign(login_config, reply)
    if(login_config.listable_names){getNicknames();}
  }
  getServerConfig(setConfig, err => console.log("getConfig Error:",err));

  //loadPlayers from options or create?
  //console.log(options)

  MTLG.lang.define({
    'en': {
      'no_nick': "Player",
      'welcome_msg': "Welcome, you are playing on the",
      'new_button': "I'm new here",
      'login_button': "Login",
      'loading': "Loading ⟳",
      'enter_pseudo':"Enter your Pseudonym",
      'no_pseudo': "I don't have a Pseudonym",
      'register_pseudo': "Register this Pseudonym",
      'writedown_pseudo': "Please write it down in case you want to play on other servers",
      'got_pseudo': "Got it!",
      'change_nick': "Change Nickname",
      'set_nick': "Set Nickname",
      'chose_nick': "Chose your Nickname",
      'change_login': "Change Login",
      'show_pseudo': "Show Pseudo",
      'account_settings': "Account Settings:",
      'close_play': "Close & Play",
      'enter': "Enter",
      'login_method': "Login-Method",
      'change_login': "Change this Login",
      'nick': "Nickname",
      'enter_nick': "Enter your Nickname",
      'no_pw_needed': "No password required",
      'goto_login': "Go to Login",
      'enter_pin': "Enter your Pin",
      'player': "Player",
      'set_nickname_reminder': "You'll also need to set a Nickname in the Account Settings"
    },
    'de': {
      'no_nick': "Spieler",
      'welcome_msg': "Willkommen, du spielst auf",
      'new_button': "Ich bin neu hier",
      'login_button': "Anmelden",
      'loading': "Lade ⟳",
      'enter_pseudo':"Gib dein Pseudonym ein",
      'no_pseudo': "Ich habe kein Pseudonym",
      'register_pseudo': "Pseudonym überprüfen",
      'writedown_pseudo': "Bitte schreib die dein Pseudonym auf, falls du auf anderen Servern spielen möchtest.",
      'got_pseudo': "Hab's!",
      'chose_nick':"Wähle deinen Nicknamen",
      'change_nick': "Nicknamen ändern",
      'set_nick': "Nicknamen ändern",
      'change_login': "Login ändern",
      'show_pseudo': "Pseudonym anzeigen",
      'account_settings': "Kontoeinstellungen",
      'close_play': "Schließen & Spielen",
      'enter': "Enter",
      'login_method': "Login-Methode",
      'change_login': "Ändere dieses Login",
      'nick': "Nickname",
      'enter_nick': "Gebe deinen Nicknamen ein",
      'no_pw_needed': "Kein Passwort b  enötigt",
      'goto_login': "Gehe zum Login",
      'enter_pin': "Gebe deine PIN ein",
      'player': "Spieler",
      'set_nickname_reminder': "Du musst auch noch einen Nickname in den Kontoeinstellungen setzten "

    }
  })
  lang = MTLG.lang.getString


  //EXAMPLE Players
  var player1 = {
    "position":1,
    "state":"start",
    "history":[],
    "area": {
      "middleX": MTLG.getOptions().width/4,
      "middleY": MTLG.getOptions().height/4,
      "sizeX": MTLG.getOptions().width/3,
      "sizeY": MTLG.getOptions().height/3,
      "rotation": 180
    }
  }
  var player2 = {
    position:2,
    state: "start",
    history: [],
    area: {
      middleX: MTLG.getOptions().width*3/4,
      middleY: MTLG.getOptions().height / 4,
      sizeX: MTLG.getOptions().width/3,
      sizeY: MTLG.getOptions().height/3,
      rotation: 180
    }
  }
  var player3 = {
    position:3,
    state: "start",
    history: [],
    area: {
      middleX: MTLG.getOptions().width/4,
      middleY: MTLG.getOptions().height *3/ 4,
      sizeX: MTLG.getOptions().width/3,
      sizeY: MTLG.getOptions().height/3,
      rotation: 0
    }
  }
  var player4 = {
    position:4,
    state: "start",
    history: [],
    area: {
      middleX: MTLG.getOptions().width*3/4,
      middleY: MTLG.getOptions().height * 3/4,
      sizeX: MTLG.getOptions().width/3,
      sizeY: MTLG.getOptions().height/3,
      rotation: 0
    }
  }




  player_logins = [player1,player2,player3,player4]


}


var login_config = {
  //idserver address
  host: 'https://mtlg.elearn.rwth-aachen.de/ident', //without http
  name: 'MTLG-Server',
  //idserver config
  listable_names: true,
  login_withoutpw: false,
  login_required: false,
  anonymously: true,
  login_methods: ['Pin','Symbols'],
  symbols: [['🥪','🍣','🍰','🍝','🍕','🍔'],['🐶','🐷','🐻','🐸'],[['🤟'],['🤙'],['🤏'],['🖖'],['🤲'],['👍'],['👎'],['🖐']]],
  symbols_categories: ["What's your favorite food?","Which animal do you like?","What's your secret hand sign?"],
  //game config
  game_id: "f13d426b-cf51-4db8-b6c2-a1a18bb11ec1",
  input_method: 'hardware', //'qr' or 'virtual' or 'hardware'
  windowcolor: 'white',
  buttoncolor1: '',
  buttoncolor2: '',
  textcolor: 'black'
}







//logic
var lang
var allNicknames = []
var player_logins = []

//Schnittstelle zum MTLG Framework
function updatePlayer_MTLG(player){
  //console.log("updating", player)
  if(player.index != undefined){
    MTLG.updatePlayer(player.index, only_pubAttr(player))
  }else{
    player.index = MTLG.addPlayer(only_pubAttr(player))
    MTLG.updatePlayer(player.index, only_pubAttr(player)) //To set player + index, cause it was undefined before adding it to MTLG
  }
  return player
}

function only_pubAttr(player){
  return {
    name: player.nickname || lang('no_nick')+(player.index+1),
    pseudonym: player.pseudonym,
    position: player.position
  }
}

//Grafisches Interface
function showLogin(){
  for(player of player_logins){
    player = showPlayersLogin(player)
  }
}

function createLoginPlayer(index,area,position){
  var player = {
    position: position,
    state: "start",
    history: [],
    area: area
  }
  player_logins[index] = showPlayersLogin(player)
}

function deleteLoginPlayer(index){
  hideLoginPlayer(index)
  player_login.splice(index, 1)

}

function showLoginPlayer(index){
  player_logins[index] = showPlayersLogin(player_logins[index] )
}

function hideLoginPlayer(index){
  player_logins[index] = hidePlayersLogin(player_logins[index] )
}



function hideLogin(){
  for(player of player_logins){
    player = hidePlayersLogin(player)
  }
}

function showPlayersLogin(player, new_state){ //new_state optional
  //removes currentPage from stage
  if(player.currentPage){
    player.emptyPage()
    MTLG.getStageContainer().removeChild(player.currentPage)
    //login_stage.removeChild(player.currentPage)
  }else{
    player = drawWindow(player)
  }

  //draws new page according to state, switch statement based on page_identifier instead of direct calls for visability
  switch(new_state || player.state){
    case "error":
      player = drawError_page(player)
      break;

    //Login to existing Account on this Server
    case "login_start":

      player = drawLoginStart_page(player)
      break;

    case "login_Pin":
      player = drawNumpadLogin_page(player)
      break;

    case "login_Symbols":
      player = drawSymbolsLogin(player)
      break;

    case "login_Name":
      player = drawNameOnlyLogin_page(player)
      break;

    //New Account on this Server
    case "new_start":
      player = drawRegister_page(player)
      break;

    //New Pseudonym
    case "show_pseudo":
      player = drawNewPseudo_page(player)
      break;


    //Logged in, manage account
    case "manage_account":
      player = drawManageAccount_page(player);
      break;

    case "change_nickname":
      player = drawChangeNickname_page(player);
      break;

    case "change_login":
      player = drawChangeLogin_page(player);
      break;

    //Startpage
    case "start":
    default:
      player = drawStart_page(player)
      break;
  }


  //adds newPage to stage
  MTLG.getStageContainer().addChild(player.currentPage)

  player.prevstate = player.state
  return player
}

function hidePlayersLogin(player){
    MTLG.getStageContainer().removeChild(player.currentPage)

    /*test
    for(let i=0; i<MTLG.getPlayerNumber() ;i++){
      console.log("Module Playerdata: ",player_logins[i])
      console.log("MTLG Playerdata: ", MTLG.getPlayer(i))
    }*/

}

function drawButton(text,sizeX,sizeY,color,textcolor,curve){
  var button = new createjs.Container()
  var background = new createjs.Shape()
  if(curve === undefined){curve=Math.sqrt(sizeX+sizeY)/2}
  background.graphics.beginFill(color).drawRoundRect(0,0,sizeX,sizeY, curve)
  background.regX = sizeX/2
  background.regY = sizeY/2
  background.shadow = new createjs.Shadow('#000', 2,2, 3);
  var text = drawTextfield(text,0.85*sizeX,0.65*sizeY,textcolor) // new createjs.Text(text, Math.round(sizeY/2.5)+"px Arial", textcolor)
  //text.textAlign = 'center'
  //text.textBaseline =  "middle"
  //text.shadow = new createjs.Shadow('darkgrey', 2,2, 5);
  button.addChild(background,text)

  return button
}

function drawTextfield(text,sizeX,sizeY,textcolor){
  size = Math.min(sizeY,2*sizeX/text.length)
  var text = new createjs.Text(text, Math.round(size)+"px Arial", textcolor)
  text.textAlign = 'center'
  text.textBaseline =  "middle"
  text.regY= -size/15
  text.shadow = new createjs.Shadow('darkgrey', 2,2, 5);
  return text
}

function drawWindow(player){
  var page = new createjs.Container()
  //Postions Container
  page.x = player.area.middleX
  page.y = player.area.middleY
  page.regX = player.area.sizeX / 2
  page.regY = player.area.sizeY / 2
  page.rotation = player.area.rotation
  var background = new createjs.Shape()
  background.graphics.beginFill("#F0F8FF").drawRoundRect(0,0, player.area.sizeX-7, player.area.sizeY-7, Math.sqrt(player.area.sizeX+player.area.sizeY)/2)
  background.shadow = new createjs.Shadow('#000', 2, 2, 5);
  page.addChild(background)

  var prevbutton = drawButton("🡰",player.area.sizeX/10,player.area.sizeY/7,'#B0C4DE','black',Math.sqrt(player.area.sizeX+player.area.sizeY)/2)
  prevbutton.x=0.84*player.area.sizeX
  prevbutton.y=player.area.sizeY/18
  prevbutton.addEventListener("click", function(){
    prevPagePlayer(player)
  },false)
  page.addChild(prevbutton)

  if(!login_config.login_required){
    var closebutton = drawButton("🞬",player.area.sizeX/10,player.area.sizeY/7,'#B0C4DE','black',Math.sqrt(player.area.sizeX+player.area.sizeY)/2)
    closebutton.x=19*player.area.sizeX/20
    closebutton.y=player.area.sizeY/18
    closebutton.addEventListener("click", function(){
      hidePlayersLogin(player)
    },false)
    page.addChild(closebutton)
  }




  player.currentPage=page
  return player
}


function getKeyboardMethod(layout){
  if(!layout){layout = "original"}
  switch(login_config.input_method){
    case 'qr':
      return new MTLG.utils.inputkeyboards.Methods.DistributedDisplayKeyboard()
      break;
    case 'hardware':
      return new MTLG.utils.inputkeyboards.Methods.HardwareKeyboard()
      break;
    case 'virtual':
    default:
      return new MTLG.utils.inputkeyboards.Methods.VirtualKeyboard(layout);
      break;

  }
}

function buttonpressed(player,button,new_state){
  button.removeEventListener("click", buttonpressed)
  goPagePlayersLogin(player, new_state)
}

function goPagePlayersLogin(player, new_state){
  player.history.push(player.state)
  player.state=new_state
  showPlayersLogin(player)
}

function prevPagePlayer(player){
  player.state=player.history.pop()

  //Symbol stages use same page, but different counter
  if (player.symbol_page > 0){
    player.symbol_page -= 1 //adjust counter
    player.secret = player.secret.slice(0, -1) //remove last entered symbol
  }

  showPlayersLogin(player)
}


//Startpage of Login
function drawStart_page(player){

   var welcome_message = drawTextfield(lang('welcome_msg')+' '+login_config.name,9*player.area.sizeX/10,player.area.sizeY/8,'black')
   welcome_message.x = player.area.sizeX/2
   welcome_message.y = player.area.sizeY/3

  //New Player Button -> go to Registerpage
  var new_button = drawButton(lang('new_button'), player.area.sizeX/2.5, player.area.sizeY/4, '#BDB76B','black')
  new_button.x = 2/7*player.area.sizeX
  new_button.y = 2*player.area.sizeY/3
  new_button.addEventListener("click", function(){
    buttonpressed(player,new_button,'new_start')
  },false)

  //Login -> go to Loginselectionpage
  var login_button = drawButton(lang('login_button'), player.area.sizeX/2.5, player.area.sizeY/4, '#5F9EA0' , 'black')
  login_button.x = 5/7*player.area.sizeX
  login_button.y = 2*player.area.sizeY/3
  login_button.addEventListener("click", function(){
    buttonpressed(player,login_button,'login_start')
  },false)




  player.currentPage.addChild(new_button,login_button,welcome_message)
  player.emptyPage = function(){
    player.currentPage.removeChild(new_button,login_button,welcome_message)

  } //Seems to fail in some scopes
  return player
}

function drawError_page(player){
  //Super bad code
  if(player.lasterror.response){
    if(player.lasterror.response.data){
      var error_text = player.lasterror.response.data.error
    }else{
      var error_text = player.lasterror
    }
  }else{
    var error_text = player.lasterror
  }

  var welcome_message = drawTextfield(''+error_text,0.95*player.area.sizeX,0.2*player.area.sizeY,'black')
  welcome_message.x = player.area.sizeX/2
  welcome_message.y = player.area.sizeY/2

  player.currentPage.addChild(welcome_message)
  player.emptyPage = function(){
    player.currentPage.removeChild(welcome_message)
  }

  return player
}

function drawLoading_page(player){
  if(player.currentPage){
    player.emptyPage()
  }
  var welcome_message = drawTextfield(lang('loading'),0.95*player.area.sizeX,0.2*player.area.sizeY,'black')
  welcome_message.x = player.area.sizeX/2
  welcome_message.y = player.area.sizeY/2

  player.currentPage.addChild(welcome_message)
  player.emptyPage = function(){
    player.currentPage.removeChild(welcome_message)
  }
  return player
}

function drawRegister_page(player){

  options = {
    x: 0.085*player.area.sizeX,
    y: 0.2*player.area.sizeY,
    w: 0.83*player.area.sizeX,
    h: player.area.sizeY/4,
    enterButton: false,
    enterWhenReceiving: true,
    placeholder: player.pseudoinput || lang('enter_pseudo'),
    callback_onChange: true,
    callback: function(txt){
      //console.log("Entered Pseudonym: ",txt)
      player.pseudoinput = txt
    } //buttonpressed(player,login_button,'login_start')
  }

  var method = getKeyboardMethod()
  var input_field =  new MTLG.utils.inputkeyboards.getInputField(options, method);

  //New Player Button -> go to Registerpage
  var new_button = drawButton(lang('no_pseudo'), player.area.sizeX/2.5, player.area.sizeY/4, '#BDB76B','black')
  new_button.x = 2/7*player.area.sizeX
  new_button.y = 2*player.area.sizeY/3
  new_button.addEventListener("click", function(){
    buttonpressed(player,new_button,'show_pseudo')
  },false)

  //Login -> go to Loginselectionpage
  var login_button = drawButton(lang('register_pseudo'), player.area.sizeX/2.5, player.area.sizeY/4, '#5F9EA0' , 'black')
  login_button.x = 5/7*player.area.sizeX
  login_button.y = 2*player.area.sizeY/3
  login_button.addEventListener("click", function(){
    if(player.pseudoinput){
      player = drawLoading_page(player)
      var callback = function (p){
        player = p
        if(player.pseudonym){
          //logged_in(player)
          buttonpressed(player,login_button,'manage_account')
        }else{
          buttonpressed(player,login_button,'new_start')
        }
      }
      checkPseudonym(player,callback)
    }
  },false)

  //Play -> hideLogin
  player.currentPage.addChild(input_field,new_button, login_button)
  player.emptyPage = function(){player.currentPage.removeChild(input_field,new_button,login_button)}  //Seems to fail in some scopes
  return player

}

function drawNewPseudo_page(player){
  //UI
  if(player.pseudonym){
    var welcome_message = drawTextfield(player.pseudonym,0.85*player.area.sizeX,0.2*player.area.sizeY,'black')
    welcome_message.x = player.area.sizeX/2
    welcome_message.y = 0.22*player.area.sizeY

    var hint_message = drawTextfield(lang('writedown_pseudo'), 0.95*player.area.sizeX,0.2*player.area.sizeY,'black')
    hint_message.x = player.area.sizeX/2
    hint_message.y = 0.4*player.area.sizeY

    var done_button = drawButton(lang('got_pseudo'), player.area.sizeX/2.5, player.area.sizeY/4, '#5F9EA0' , 'black')
    done_button.x =  5/7*player.area.sizeX
    done_button.y = 2/3*player.area.sizeY
    done_button.addEventListener("click", function(){
      buttonpressed(player,done_button,'manage_account')
    },false)

    player.currentPage.addChild(done_button,hint_message,welcome_message)
    player.emptyPage = function(){
      player.currentPage.removeChild(done_button,hint_message,welcome_message)
    }

  }else{
    player = drawLoading_page(player)

    var callback = function(p){
      player = p
      goPagePlayersLogin(player, "show_pseudo")
    }
    newPseudonym(player,callback)
  }

  return player
}

function drawManageAccount_page(player){
  //Change Nickname
  var name_button = drawButton(player.nickname ? lang('change_nick') : lang('set_nick'), player.area.sizeX/4, player.area.sizeY/4,'#5F9EA0','black')
  name_button.x = 0.15*player.area.sizeX
  name_button.y = 0.35*player.area.sizeY
  name_button.addEventListener("click", function(){
    buttonpressed(player,name_button,'change_nickname')
  },false)

  //Change Login
  var login_button = drawButton(lang('change_login'), player.area.sizeX/4, player.area.sizeY/4, '#5F9EA0' , 'black')
  login_button.x = 0.42*player.area.sizeX
  login_button.y = 0.35*player.area.sizeY
  login_button.addEventListener("click", function(){
    buttonpressed(player,login_button,'change_login')
  },false)

  //Show Pseudonym
  var pseudo_button = drawButton(lang('show_pseudo'), player.area.sizeX/4, player.area.sizeY/4, '#5F9EA0' , 'black')
  pseudo_button.x = 0.69*player.area.sizeX
  pseudo_button.y = 0.35*player.area.sizeY
  pseudo_button.addEventListener("click", function(){
    buttonpressed(player,pseudo_button,'show_pseudo')
  },false)

  var welcome_message = drawTextfield(lang('account_settings'),0.5*player.area.sizeX,0.2*player.area.sizeY,'black')
  welcome_message.x = 0.27 * player.area.sizeX
  welcome_message.y = 0.11 * player.area.sizeY

  var play_button = drawButton(lang('close_play'), player.area.sizeX/3, player.area.sizeY/4, '#BDB76B' , 'black')
  play_button.x = 0.8*player.area.sizeX
  play_button.y = 0.8*player.area.sizeY
  play_button.addEventListener("click", function(){
    hidePlayersLogin(player)
  },false)

  player.currentPage.addChild(welcome_message, play_button,name_button, login_button, pseudo_button)
  player.emptyPage = function(){player.currentPage.removeChild(welcome_message, play_button,name_button, login_button, pseudo_button)}
  return player
}

function drawChangeNickname_page(player){
  player.nicknameinput = player.nickname
  var options = {
    x: 0.085*player.area.sizeX,
    y: 0.2*player.area.sizeY,
    w: 0.83*player.area.sizeX,
    h: player.area.sizeY/4,
    enterButton: false,
    enterWhenReceiving: true,
    placeholder: player.nickname || lang('chose_nick'),
    callback_onChange: true,
    callback: function(txt){
      //console.log("Entered Nickname: ",txt)
      player.nicknameinput = txt
    } //buttonpressed(player,login_button,'login_start')
  }

  var method = getKeyboardMethod()
  var input_field =  new MTLG.utils.inputkeyboards.getInputField(options, method);

  var change_button = drawButton(lang('enter'), player.area.sizeX/2.5, player.area.sizeY/4, '#5F9EA0' , 'black')
  change_button.x = 5/7*player.area.sizeX
  change_button.y = 2*player.area.sizeY/3
  change_button.addEventListener("click", function(){
    var callback = function (p){
      buttonpressed(p,change_button,'manage_account')
    }
    setNickname(player,callback)
  },false)

  player.currentPage.addChild(input_field,change_button)
  player.emptyPage = function(){player.currentPage.removeChild(input_field,change_button)}
  return player
}

function drawChangeLogin_page(player){
  var method_conf = {
    sizeX: 0.25*player.area.sizeX,
    sizeY: 0.2*player.area.sizeY,
    firstColor: "black",
    secondColor: "white",
    options: login_config.login_method_names || login_config.login_methods,
    defaultVal: Math.round(login_config.login_methods.length/2)-1, //if empty throw error ,((login_config.login_methods>0) ? Math.round(login_config.login_methods/2)-1 : 0),
    description: lang('login_method')
  }
  player.method=login_config.login_methods[Math.round(login_config.login_methods.length/2)-1]

  var method_scrollbox = MTLG.utils.uiElements.addScrollbox(method_conf, function(i){player.method=login_config.login_methods[i];})
  method_scrollbox.x = 0.2 * player.area.sizeX
  method_scrollbox.y = 0.15  * player.area.sizeY

  var login_button = drawButton(lang('change_login'), 0.2*player.area.sizeX, 0.2*player.area.sizeY, '#5F9EA0' , 'black')
  login_button.x = 0.65*player.area.sizeX
  login_button.y = 0.55*player.area.sizeY
  login_button.addEventListener("click", function(){
    buttonpressed(player,login_button,'login_'+player.method)

  }, false)

  player.currentPage.addChild(method_scrollbox,login_button)
  player.emptyPage = function(){player.currentPage.removeChild(method_scrollbox,login_button)}
  return player
}

function drawLoginStart_page(player){
  if(login_config.listable_names && allNicknames.length>0){
    var names = allNicknames
    if(!player.nicknameindex){
      player.nicknameindex = Math.round(names.length/2)-1
      player.nicknameinput = names[player.nicknameindex]
    }
    var conf = {
      sizeX: 0.25*player.area.sizeX,
      sizeY: 0.2*player.area.sizeY,
      firstColor: "black",
      secondColor: "white",
      options: names,
      defaultVal: player.nicknameindex, //checked if empty before((names.length>0) ? Math.round(names.length/2)-1 : 0),
      description:  lang['nick']
     }

    var names_input = MTLG.utils.uiElements.addScrollbox(conf, function(i){player.nicknameinput=names[i];player.nicknameindex=i;})
    names_input.x = 0.04 * player.area.sizeX
    names_input.y = 0.15 * player.area.sizeY



  }else{
    var welcome_message = drawTextfield( lang('nick'),0.3*player.area.sizeX,0.15*player.area.sizeY,'black')
    welcome_message.x = 0.34*player.area.sizeX
    welcome_message.y = 0.1*player.area.sizeY

    options = {
      x: 0.04*player.area.sizeX,
      y: 0.44*player.area.sizeY,
      w: 0.32*player.area.sizeX,
      h: 0.22*player.area.sizeY,
      enterButton: false,
      enterWhenReceiving: true,
      placeholder: player.nicknameinput || lang('enter_nick'),
      callback_onChange: true,
      callback: function(txt){
        //console.log("Entered Nickname: ",txt)
        player.nicknameinput = txt
      } //buttonpressed(player,login_button,'login_start')
    }

    var method = getKeyboardMethod()
    var names_input =  new MTLG.utils.inputkeyboards.getInputField(options, method);
  }
  if(login_config.login_withoutpw){
    player.method='Name'
    var  method_scrollbox = drawTextfield(lang['no_pw_needed'],0.25*player.area.sizeX,0.2*player.area.sizeY,'black')
     method_scrollbox.x = 0.55*player.area.sizeX
     method_scrollbox.y = 0.55*player.area.sizeY
  }else{
    player.method=login_config.login_methods[Math.round(login_config.login_methods.length/2)-1]
    var method_conf = {
      sizeX: 0.25*player.area.sizeX,
      sizeY: 0.2*player.area.sizeY,
      firstColor: "black",
      secondColor: "white",
      options: login_config.login_method_names || login_config.login_methods,
      defaultVal: Math.round(login_config.login_methods.length/2)-1, //if empty throw error ,((login_config.login_methods>0) ? Math.round(login_config.login_methods/2)-1 : 0),
      description: lang('login_method')
     }
    var method_scrollbox = MTLG.utils.uiElements.addScrollbox(method_conf, function(i){player.method=login_config.login_methods[i]; })
    method_scrollbox.x = 0.4 * player.area.sizeX
    method_scrollbox.y = 0.15  * player.area.sizeY
  }

  var login_button = drawButton(lang('goto_login'), 0.2*player.area.sizeX, 0.2*player.area.sizeY, '#5F9EA0' , 'black')
  login_button.x = 0.85*player.area.sizeX
  login_button.y = 0.55*player.area.sizeY
  login_button.addEventListener("click", function(){
    if(player.method && player.nicknameinput){buttonpressed(player,login_button,'login_'+player.method)}
  },false)

  player.currentPage.addChild(names_input,method_scrollbox,login_button)
  player.emptyPage = function(){player.currentPage.removeChild(names_input,method_scrollbox,login_button)}



  //Display NameInput
    //inputfield
    //NameList with getAllNames from identserver

  //Display PasswordInput for Loginmethod
    //displayNumpad
    //displaySympad


  //After succesful login -> logged in page
  //
  return player
}

function drawNameOnlyLogin_page(player){

}

function drawNumpadLogin_page(player){
  var options = {
    x: 0.1*player.area.sizeX,
    y: 0.45*player.area.sizeY,
    w: 0.5*player.area.sizeX,
    h: 0.3*player.area.sizeY,
    enterButton: false,
    enterWhenReceiving: true,
    placeholder:lang('enter_pin'),
    callback_onChange: true,
    callback: function(txt){
      //console.log("Entered Pin: ",txt)
      player.secret = hash(txt) //hashed
    }, //buttonpressed(player,login_button,'login_start')
    password: true
  }

  var method = getKeyboardMethod("numbers")
  var input_field =  new MTLG.utils.inputkeyboards.getInputField(options, method);

  var welcome_message = drawTextfield(player.nicknameinput ? lang('player')+": "+player.nicknameinput : lang('set_nickname_reminder') ,0.8*player.area.sizeX,0.2*player.area.sizeY,'black')
  welcome_message.x = player.area.sizeX/2
  welcome_message.y = 0.25*player.area.sizeY

  var login_button = drawButton(lang('login_button'), 0.3*player.area.sizeX, 0.3*player.area.sizeY, '#5F9EA0' , 'black') //lang
  login_button.x = 0.75*player.area.sizeX
  login_button.y = 0.6*player.area.sizeY
  login_button.addEventListener("click", function(){
    if(player.secret){
      player = drawLoading_page(player)

      var callback = function(p){

        buttonpressed(p,login_button,"manage_account")
      }
      if(player.pseudonym){
        setLogin(player, callback)
      }else{
        checkLogin(player,callback)
      }
    }
  },false)

  player.currentPage.addChild(welcome_message,input_field,login_button)
  player.emptyPage = function(){player.currentPage.removeChild(welcome_message,input_field,login_button)}
  return player
}

function drawSymbolsLogin(player){


  player.symbol_page = player.symbol_page || 0

  if(player.symbol_page < login_config.symbols.length){
    if(player.symbol_page == 0) {player.secret = ''} //in case player already entered some pin etc. but aborted

    var welcome_message = drawTextfield(login_config.symbols_categories[player.symbol_page],0.7*player.area.sizeX,0.2*player.area.sizeY,'black')
    welcome_message.x = 0.4*player.area.sizeX
    welcome_message.y = 0.1*player.area.sizeY
    var symbol_container = drawSymbolSet(player)


    player.currentPage.addChild(welcome_message,symbol_container)
    player.emptyPage = function(){player.currentPage.removeChild(welcome_message,symbol_container)}
  }else{
    //Done with all symbol pages
    player.secret = hash(player.secret)

    player = drawLoading_page(player)

    var callback = function(p){
      goPagePlayersLogin(p,'manage_account')
      //buttonpressed(p,login_button,'manage_account')
    }
    if(player.pseudonym){
      setLogin(player, callback)
    }else{
      checkLogin(player,callback)
    }
    //In case player needs to restart login
    for(var i=1;i<login_config.symbols.length;i++){player.history.pop() } //removes all but one symbolpage from the history

    player.symbol_page = 0

  }

  return player
}

function drawSymbolSet(player){
  var symbol_container = new createjs.Container()

  var symbols = login_config.symbols[player.symbol_page]

  //we want sizeX/sizeY = rows/columns with rows*columns > number of symbols
  // math
  // => rows > sqrt( number of symbols * sizeX/sizeY)
  var rows = Math.round(Math.sqrt(symbols.length*((player.area.sizeX)/(player.area.sizeY))))
  var columns = Math.round(symbols.length/rows)
  rows = Math.round(symbols.length/columns)
  if(rows*columns < symbols.length){(0.8*player.area.sizeX) > (0.7*player.area.sizeY) ? rows++ : columns++}


  var buttonsize = 0.9*Math.min(0.8*player.area.sizeX/rows,0.7*player.area.sizeY/columns)
  var symbols_boundsX = 0.8*player.area.sizeX-buttonsize
  var symbols_boundsY = 0.7*player.area.sizeY-buttonsize

  var i = 0;
  for (var c = 0; c < columns; c++){
    if(i == symbols.length) {break;}
    for (var r = 0; r < rows; r++){
      if(i == symbols.length) {break;}
      (function(){
        var symbol = symbols[i]
        var symbol_button = drawButton(symbol, buttonsize, buttonsize, 'white','black',buttonsize/6)
        symbol_button.x = (rows-1 ? (r/(rows-1)) : 0.5 ) * symbols_boundsX
        symbol_button.y = (columns-1 ? (c/(columns-1)) : 0.5) * symbols_boundsY

        symbol_button.addEventListener("click", function(evt){
          player.symbol_page++
          player.secret = player.secret + symbol //evt.target.text // should be symbol isntead of evt.target.text, but weird scope issue #workaround
          buttonpressed(player,symbol_button,'login_Symbols')
        },false)
        symbol_container.addChild(symbol_button)
        i++;
      }());
    }
  }
  symbol_container.x=0.1*player.area.sizeX+buttonsize/2
  symbol_container.y=0.2*player.area.sizeY+buttonsize/2
  return symbol_container

}






//Pseudonym & Login Interface to ID-Server
//-------------------------------------------------------------------------------------------------------
function hash(value,retries){
  var retries_ = retries || 3 //hashing fails sometimes
  var hashed_value
  try{
    hashed_value = sha256.default(value)
  }catch(err){
    console.log("Hashing Error: ",err)
    hashed_value = retries_ >0 ? hash(value,retries_-1) : 'Error in sha256'
  }
  //hash funktion
  return hashed_value
  //return secret
}


//Pseudonym -> neu -> Nickname, PW setzten                                                        = Nickname & Pseudonym known
function newPseudonym(player,callback){
  var cb = function(data){
    player.pseudonym= data.pseudonym
    player.pseudoinput= player.pseudonym
    player = updatePlayer_MTLG(player)
    callback(player)
  }

  //Callback if error occurs
  var errorHandler = function(err){
    player.lasterror = err

    prevPagePlayer(player)
    goPagePlayersLogin(player, "error")
  }

  //makes sure player can only generate one pseudonym
  if(player.pseudonym){
    cb({pseudonym: player.pseudonym})
  }else{
    createPlayerRequest(cb,errorHandler)
  }

 //nickname optional? test only
  //cb({pseudonym: "TestPseudo"+Math.round(Math.random()*100)})
}

//not used?
//          -> bestehend (nicht init) -> Eingabe -> Gültigkeit prüfen -> Nickname, PW setzten     = Nickname & Pseudonym known
function resetPseudonym(pseudo, nickname, pw){
  pseudo = sendPseudo(pseudo, nickname, pw)
  return (pseudonym, nickname)
}

//          -> bestehend (init) -> Eingabe -> Gültigkeit prüfen -> return Nickname                = Nickname & Pseudonym known
function checkPseudonym(player,callback){
  //Callback if succesful
  var cb = function(data){
    player.pseudonym = data.pseudonym
    player.pseudoinput= player.pseudonym
    player.nickname = data.nickname
    player.nicknameinput = player.nickname
    player = updatePlayer_MTLG(player)
    callback(player)
  }

  //Callback if error occurs
  var errorHandler = function(err){
    player.lasterror = err
    goPagePlayersLogin(player, "error")
  }

  loginPseudoRequest(cb, errorHandler, player.pseudoinput)

  //just for testing
  //if(player.pseudoinput == 'abcd'){cb({pseudonym: "abcd"})}
}

//Nickname & pw  -> Gültigkeit prüfen -> return Pseudonym                                          = Nickname & Pseudonym known
function checkLogin(player,callback){
  var cb = function(data){
    player.pseudonym = data.pseudonym //reply.pseudonym should be here
    player.pseudoinput = player.pseudonym
    player.nickname = data.nickname
    player.nicknameinput = player.nickname
    player = updatePlayer_MTLG(player)
    callback(player)
  }

  var errorHandler = function(err){
    player.lasterror = err
    goPagePlayersLogin(player, "error")
  }

  //console.log("Check Login for Player: ", player)
  loginMethodRequest(cb, errorHandler, player.nicknameinput, player.method, player.secret)
  //createLoginRequest(cb, errorHandler, player.nicknameinput, player.method, player.secret)
}

function setNickname(player, callback){
  var cb = function(data){
    player.nickname = data.nickname
    player.nicknameinput = player.nickname
    player = updatePlayer_MTLG(player)
    callback(player)
  }

  //Callback if error occurs
  var errorHandler = function(err){
    player.lasterror = err
    goPagePlayersLogin(player, "error")
  }

  setNicknameRequest(cb, errorHandler, player.pseudonym, player.nicknameinput)
}

function setLogin(player, callback){
  //console.log("Set Login for Player: ", player)
  var cb = function(data){
    callback(player)
  }

  //Callback if error occurs
  var errorHandler = function(err){
    console.log(err)
    player.lasterror = err
    goPagePlayersLogin(player, "error")
  }

  setLoginMethodRequest(cb, errorHandler, player.pseudonym, player.method, player.secret)
}

function getNicknames(){
  getPlayersRequest(function(data){allNicknames=data.players}, err => console.log("GetNicknames error", err)) ///insert right parameter name here
}



//Network
//--------------------------------------------------------------------------------------------------
//Send generic msg to ID-Server
function getRequest(path, data, callback, errorHandler){
  axios
    .get(login_config.host+path, data)
    .then(reply =>{callback(reply.data)})
    .catch(err => {errorHandler(err)})
}

function postRequest(path, data, callback, errorHandler){
  //console.log(data)
  axios
    .post(login_config.host+path, data)
    .then(reply =>{callback(reply.data)})
    .catch(err => {errorHandler(err)})
}

function putRequest(path, data, callback, errorHandler){
  axios
    .put(login_config.host+path, data)
    .then(reply =>{callback(reply.data)}) //just callback should be enough?
    .catch(err => {errorHandler(err)})
}

function deleteRequest(path, data, callback, errorHandler){
  axios
    .delete(login_config.host+path, data)
    .then(reply =>{callback(reply.data)})
    .catch(err => {errorHandler(err)})
}

//Path: /manage/players/
function getPlayersRequest(callback, errorHandler){
  data= {}

  getRequest("/manage/players/",data,callback, errorHandler)
}

//Path: /manage/player/
//Request new Pesudonym from ID-Server and set Login
function createPlayerRequest(callback, errorHandler, nickname){
  data = {"nickname": nickname}

  putRequest("/manage/player/", data, reply => {callback(reply)}, errorHandler) //just callback should be enough?
}

//not in use?
function deletePlayerRequest(callback, errorHandler, pseudo){
  data = {"pseudonym": pseudo}

  deleteRequest("/manage/player/", data, reply => {callback(reply)}, errorHandler) //just callback should be enough?
}

//Path: /manage/login/
function createLoginRequest(callback, errorHandler, nickname, login_method, login_value){ //<-"#/components/schemas/CreateLoginRequest"
  data = {
    "nickname": nickname,
    "login_method":login_method,
    "login_value":login_value
  }

  putRequest("/manage/login/", data, reply => {callback(reply)}, errorHandler) //just callback should be enough?

}

//Path: /manage/login/method/
//Set or Resets LoginMethod
function setLoginMethodRequest(callback, errorHandler, pseudo, method, secret){
  data = {
    "pseudonym": pseudo,
    "method": method,
    "value": secret
  }
  //console.log("setLogin data:",data)
  putRequest("/manage/login/method/", data , callback, errorHandler)
}

function deleteLoginMethodRequest(callback, errorHandler, pseudo, method){
  data = {
    "pseudonym": pseudo,
    "method": method
  }

  deleteRequest("/manage/login/method/", data , callback, errorHandler)
}

//Path: /manage/setNickname/
function setNicknameRequest(callback, errorHandler, pseudo, nickname){
  data = {
    "pseudonym": pseudo,
    "newNick": nickname
  }
  //console.log("newNick data:",data)
  putRequest("/manage/setNickname", data , callback, errorHandler)
}


//Path: /login/pseudonym/
//Send Psudo
function loginPseudoRequest(callback, errorHandler, pseudo){
  data = {
    "pseudonym": pseudo
  }
  postRequest("/login/pseudonym/", data, callback, errorHandler)

}

//Path: /login/method/
function loginMethodRequest(callback, errorHandler, nickname, method, value){ //<-"#/components/schemas/CreateLoginRequest"
  data = {
    "nickname": nickname,
    "method": method,
    "value": value
  }
  postRequest("/login/method/", data, callback, errorHandler)
}

function getServerConfig(callback, errorHandler){
  getRequest("/config/"+login_config.game_id, {}, callback, errorHandler)
}


//-----------------------------------
//MTLG-Framework Integration
function info() {
  return {
    name: 'login modul',
    note: 'MTLG-Modul that enables researchers to combine learning data anonymously across multiple sessions'
  }
}

MTLG.login = {
  init: init,
  info: info,
  showLogin: showLogin,
  showLoginPlayer: showLoginPlayer,
  createLoginPlayer: createLoginPlayer,
  deleteLoginPlayer: deleteLoginPlayer,
  hideLoginPlayer: hideLoginPlayer,
  //Better Naming, but still compatible
  showLoginWindow: showLoginPlayer,
  createLoginWindow: createLoginPlayer,
  deleteLoginWindow: deleteLoginPlayer,
  hideLoginWindow: hideLoginPlayer,
  hideLogin: hideLogin
};
MTLG.addModule(init, info);
